//import Link from 'next/link'
import style from './Author.module.css'
import Tag from './Tag'

export default function Tags({ version, date, tags=[] }) {

  const purple = {
    background: 'var(--satin)',
    color: 'var(--royal)',
    padding: '0.2em',
    borderRadius: '0.2em'
  }

    const renderTags = () => {
        return tags.map(({ tag }, index) => (
          <Tag target={tag} key={index} />
        ));
      };

    return (
        <div className={style.dateFlex}>
            <div>
                {renderTags()}
            </div>
        </div>
    )
  }

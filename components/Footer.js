import style from './Footer.module.css'
import EmailSignup from './emailSignup'

export default function Footer() {
    return (
        <div className={style.footerbg}>
        <div className="WritingGrid12">
            <div className="seven-eight">
                <div className={style.ctaholding}>
                    <h5>Want Updatesç?</h5>
                    <br/>
                    <p className="smallT">
                      Join the email list and we'll send you updates on the Xalgorithms Foundation, our projects, writings, and ideas. 
                    </p>
                    {/*
                    <br/>
                    <InputText placeholder="Sign up for updates"/>
                    */}
                    <EmailSignup/>
                  </div>
            </div>
        </div>
            <div className={style.footerhold}>
                <div>
                    <p>
                    Text Graphics CC-by International 4.0  
                    <br />
                    Core Software Apache 2.0 
                    <br />
                    Extensions Affero GPL 3.0
                    </p>
                </div>
                <div className={style.linkgridvar}>
                    <div>
                        <a href="https://xalgo-system.herokuapp.com/" target="_blank" className={style.footerlink}>
                            XRM dev
                        </a>
                    </div>
                    <div>
                        <a href="https://development.xalgorithms.org/" target="_blank" className={style.footerlink}>
                            Developer Docs
                        </a>
                        <a href="https://gitlab.com/xalgorithms-alliance" target="_blank" className={style.footerlink}>
                            Source Code
                        </a>
                        <a href="https://suspicious-goodall-5c206e.netlify.app/" target="_blnk" className={style.footerlink}>
                            Design and Brand
                        </a>
                    </div>
                    <div>
                        <a href="https://twitter.com/Xalgorithms" target="_blank" className={style.footerlink}>
                            Twitter
                        </a>
                        <a href="https://www.linkedin.com/company/11028670/admin/" target="_blank" className={style.footerlink}> 
                            LinkedIn
                        </a>
                        <a href="mailto:jpotvin@xalgorithms.org " target="_blank" className={style.footerlink}>
                            Email
                        </a>
                    </div>
                    <div>
                        <a className={style.footerlink}>
                            Donate
                        </a>
                        <a href="https://wordpress-309334-1044070.cloudwaysapps.com/" target="_blank" className={style.footerlink}>
                            Archive
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}
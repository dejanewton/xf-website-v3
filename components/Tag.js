import React, { useRef, useEffect, useContext } from 'react'
import tagSelect from '../lib/tagSelect'
import Link from 'next/link'

export default function Tag ({target}) {
    

    const era = {
        background: 'var(--clear)',
        color: 'var(--naval)',
        padding: '0.2em',
        borderRadius: '0.2em',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1.6em',
        boxShadow: '0 0 4px 4px var(--clear)',
        border: 'none',
        cursor: 'pointer'
    }

    const xrmDev = {
        background: 'var(--clear)',
        color: 'var(--naval)',
        padding: '0.2em',
        borderRadius: '0.2em',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1.6em',
        boxShadow: '0 0 4px 4px var(--clear)',
        border: 'none',
        cursor: 'pointer'
    }

    const standard ={
        background: 'var(--clear)',
        color: 'var(--naval)',
        padding: '0.2em',
        borderRadius: '0.2em',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1.6em',
        boxShadow: '0 0 4px 4px var(--clear)',
        border: 'none',
        cursor: 'pointer'
    }

    const [tagType, setTagType] = React.useState(standard)

    function pickStyle (ref) {
        useEffect(() => {
            if (target === 'era') {
                setTagType(era)
            } else if (target === 'xrm-dev') {
                setTagType(xrmDev)
            } else {
                setTagType(standard)
            }
        }, [ref])
    }

    const wrapperRef = useRef(null)
    pickStyle(wrapperRef)

    const { tag, filterTag } = useContext(
        tagSelect
      );

    const choose = () => {
        filterTag(target)
    }


    return (
        <Link href={'/writing'}>
            <button 
            ref={wrapperRef} 
                style={tagType} 
                onClick={
                    choose
                }>
                    {target}
            </button>
        </Link>
    )
}
import React, {  useEffect, useRef, useState } from 'react';

export default function RandomLogo() {

    const width = {
        width: '100%'
    }

    const ref = useRef(null);

  const [sketchWidth, setSketchWidth] = useState(0)
  const [sketchHeight, setSketchHeight] = useState(0)

  const renderLogo = () => {
    useEffect(() => {
      const Sketch = require("p5")
      const {Vector} = require("p5")
      const s = ( Sketch ) => {
        var vehicles = [];

        setSketchWidth(ref.current ? ref.current.offsetWidth : 0)
        setSketchHeight(ref.current ? ref.current.offsetWidth : 0)

        const w = sketchWidth,
              h = sketchHeight,
              wa = 0.333*w,
              wb = 0.5*w,
              wc = 0.666*w,
              bsa = 0.15*w,
              bsb = 0.108*w,
              bsc = 0.058*w;

        Sketch.setup = () => {
          Sketch.createCanvas(w, h);
        };

        var la = new Vehicle(wa, wa, bsa);
        var lb = new Vehicle(wa, wb, bsb);
        var lc = new Vehicle(wa, wc, bsa);
        var ma = new Vehicle(wb, wa, bsb);
        var mb = new Vehicle(wb, wb, bsa);
        var mc = new Vehicle(wb, wc, bsc);
        var ra = new Vehicle(wc, wa, bsa);
        var rb = new Vehicle(wc, wb, bsc);
        var rc = new Vehicle(wc, wc, bsb);
        vehicles.push(la);
        vehicles.push(lb);
        vehicles.push(lc);
        vehicles.push(ma);
        vehicles.push(mb);
        vehicles.push(mc);
        vehicles.push(ra);
        vehicles.push(rb);
        vehicles.push(rc);

        Sketch.draw = () => {
          Sketch.background(260);

          for (var i = 0; i < vehicles.length; i++) {
            var v = vehicles[i];
            v.show();
          }
        }

        function Vehicle(x,y,d) {
          this.pos = Sketch.createVector(x, y);
          this.target = Sketch.createVector(x,y);
          this.vel = Sketch.createVector();
          this.acc = Sketch.createVector();
          this.r = 5;
          this.rad = d;
          this.maxspeed = 3;
          this.maxforce = 2;
      }
    
    
    
    
    Vehicle.prototype.show = function() {
          var r = r++;
          Sketch.push();
            Sketch.ellipseMode(Sketch.CENTER);
            Sketch.fill(0);
            Sketch.translate(this.pos.x, this.pos.y);
            let rSize = 16;
            var shapex = this.rad;
            var shapey = this.rad;
            Sketch.ellipse(10, 10, shapex, shapey);
          Sketch.pop();
        }
    

      }
      let myp5 = new Sketch(s, 'generateLogo');
    }, [ref.current])
  }

  // const wrapperRef = useRef(null)
  renderLogo()
  

    return (
      <div style={width} ref={ref}>
        <div id="generateLogo"/>
      </div>
    )
  }
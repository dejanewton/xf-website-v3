import style from './Caption.module.css'

export default function Caption({children}) {
    return(
        <div>
            <p className={style.caption}>
                {children}
            </p>
        </div>
    )
}
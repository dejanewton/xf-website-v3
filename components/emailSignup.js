import { Button } from 'xf-material-components/package/index'

export default function EmailSignup() {

    const topper = {
        marginTop: '1em'
    }

    return(
        <div style={topper}>
            <a href="https://lists.xalgorithms.org/mailman/listinfo/community" target="_blank"><Button>Sign up</Button></a>
        </div>
    )
}